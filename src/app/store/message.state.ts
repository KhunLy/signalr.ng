import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { Message } from "../models/message";

export interface MessageStateModel {
    messages: Message[];
}

export class AddMessage {
    static readonly type = '[MessageState] add message';
    constructor(public message: Message) {}
}

@State<MessageStateModel>({
    name: 'Message',
    defaults: {
        messages: []
    }
})
@Injectable()
export class MessageState {

    @Selector()
    static messages(state: MessageStateModel) {
        return state.messages;
    }

    @Action(AddMessage)
    addMessage(ctx: StateContext<MessageStateModel>, action: AddMessage) {
        //l'état du state avant les changements
        let state: MessageStateModel = ctx.getState();
        // l'état du state après
        ctx.setState({ messages: [...state.messages, action.message] });
    }
}