import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import * as signalr from '@microsoft/signalr';
import { Store } from '@ngxs/store';
import { AddMessage } from '../store/message.state';

@Injectable({
  providedIn: 'root'
})
export class MessageService {


  connection: signalr.HubConnection;

  constructor(
    private store: Store
  ) 
  { 
      this.connection = new signalr.HubConnectionBuilder()
        .withUrl('http://khun.somee.com/ws/message').build();
      this.connection.start();
      this.connection.on('MessageRecieved', m => this.store.dispatch(new AddMessage(m)));
  }

  sendMessage(message: Message) {
    this.connection.invoke('SendMessage', message);
  }

}
