import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Message } from 'src/app/models/message';
import { MessageService } from 'src/app/services/message.service';
import { MessageState } from 'src/app/store/message.state';

@Component({
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {

  @Select(MessageState.messages) message$!: Observable<Message[]>

  messages: Message[] = [];

  newMessage!: string;

  constructor(
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.message$.subscribe(messages => this.messages = messages);
  }

  ngOnDestroy() { }

  send() {
    this.messageService.sendMessage({ author: 'Khun', content: this.newMessage });
    this.newMessage = '';
  }

}
